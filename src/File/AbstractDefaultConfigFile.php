<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Config
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Config\File;

//
use Jantia\Asi\Helper\AsiConfigHelperTrait;
use Tiat\Config\Exception\InvalidArgumentException;
use Tiat\Standard\Config\ConfigFileInterface;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Register\RegisterContainerInterface;
use Tiat\Stdlib\Register\RegisterHandler;

use function array_diff;
use function array_flip;
use function array_keys;
use function array_values;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDefaultConfigFile extends RegisterHandler implements ConfigInterface, ConfigFileInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiConfigHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array ACCEPTED_CONFIG_OPTIONS = ['paramValidation' => NULL, 'override' => NULL];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string VALIDATOR_CLASS = 'class';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string VALIDATOR_OPTIONS = 'options';
	
	/**
	 * @var array|null[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_validatorInstance = [self::VALIDATOR_CLASS => NULL, self::VALIDATOR_OPTIONS => NULL];
	
	/**
	 * @var RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	private RegisterInterface $_registerInterface;
	
	/**
	 * @param ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		parent::__construct($params, $autoinit, $autorun, ...$args);
		
		//
		$this->registerConfig(...$params);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function registerConfig(...$args) : static;
	
	/**
	 * @param    null|iterable    $params
	 * @param                     ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function __invoke(iterable $params = [], ...$args) : ClassLoaderInterface {
		//
		$this->registerConfig(...$args);
		
		//
		return $this;
	}
	
	/**
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterInterface() : ?RegisterInterface {
		return $this->_registerInterface ?? NULL;
	}
	
	/**
	 * @param    RegisterInterface    $register
	 *
	 * @return ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterInterface(RegisterInterface $register) : ConfigFileInterface {
		//
		$this->_registerInterface = $register;
		
		//
		return $this;
	}
	
	/**
	 * @return ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRegisterInterface() : ConfigFileInterface {
		//
		unset($this->_registerInterface);
		
		//
		return $this;
	}
	
	/**
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    array                                                $values
	 * @param    array                                                $options
	 * @param    mixed                                                ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function registerConfigValues(RegisterPluginInterface&ParametersPluginInterface $register, array $values, array $options = [], ...$args) : void {
		if(! empty($values)):
			foreach($values as $key => $val):
				if($val instanceof RegisterContainerInterface):
					$register->setRegisterContainer($val);
				elseif(( $option = $options[$key] ?? NULL ) !== NULL):
					if(! empty($compare = $this->_checkConfigValueOptions($option))):
						$this->_registerConfigValue($register, $key, $val, ...$compare);
					endif;
				elseif($register->hasParam($key) === TRUE):
					$msg = sprintf("Register has the value for key %s with value %s but override is denied", $key,
					               (string)$register->getParam($key));
					throw new InvalidArgumentException($msg);
				else:
					// If no options defined or param don't exist.
					$this->_registerConfigValue($register, $key, $val, ...$args);
				endif;
			endforeach;
		endif;
	}
	
	/**
	 * @param    array    $options
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkConfigValueOptions(array $options) : ?array {
		//
		$from   = array_keys($options);
		$accept = array_keys(self::ACCEPTED_CONFIG_OPTIONS);
		$result = array_flip(array_values(array_diff($from, array_diff($accept, $from))));
		
		//
		return [...$result, ...$options];
	}
	
	/**
	 * @param    RegisterPluginInterface&ParametersPluginInterface    $register
	 * @param    int|string                                           $key
	 * @param    mixed                                                $value
	 * @param                                                         ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _registerConfigValue(RegisterPluginInterface&ParametersPluginInterface $register, int|string $key, mixed $value, ...$args) : void {
		$register->setParam($key, $value, ...$args);
	}
	
	/**
	 * @param    array    $default
	 * @param    array    $args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function mergeConfigValues(array $default, array $args = []) : array {
		return [...$default, ...$args];
	}
	
	/**
	 * @param    string    $class
	 * @param    array     $options
	 *
	 * @return array|null[]
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setValidator(string $class, array $options = []) : array {
		$instance                          = $this->_validatorInstance;
		$instance[self::VALIDATOR_CLASS]   = $class;
		$instance[self::VALIDATOR_OPTIONS] = $options;
		
		return $instance;
	}
}
