<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Config
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Config\Loader;

//
use Tiat\Config\Config;
use Tiat\Config\Exception\InvalidArgumentException;
use Tiat\Config\Exception\RuntimeException;
use Tiat\Standard\Config\ConfigFileInterface;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Standard\Config\ConfigLoaderInterface;

use function class_exists;
use function file_exists;
use function get_debug_type;
use function is_callable;
use function is_iterable;
use function is_string;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait ConfigLoader {
	
	/**
	 * @var ConfigInterface
	 * @since   3.0.0 First time introduced.
	 */
	private ConfigInterface $_config;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_configFilename;
	
	/**
	 * @return null|ConfigFileInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfig() : ?ConfigInterface {
		return $this->_config ?? NULL;
	}
	
	/**
	 * @param    ConfigInterface    $config
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfig(ConfigInterface $config) : ConfigLoaderInterface {
		//
		$this->_config = $config;
		
		//
		return $this;
	}
	
	/**
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfig() : ConfigLoaderInterface {
		//
		unset($this->_config);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigFile() : ?string {
		return $this->_configFilename ?? NULL;
	}
	
	/**
	 * @param    string    $filename
	 * @param    bool      $autoload
	 * @param    bool      $overwrite
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfigFile(string $filename, bool $autoload = TRUE, bool $overwrite = FALSE) : ConfigLoaderInterface {
		// Don't test class with class_exists() because it will declare it with autoload
		if($overwrite === TRUE || ( $overwrite === FALSE && $this->checkConfigFile() === FALSE )):
			// Don't check the file itself, just trust the input
			if($autoload === TRUE):
				$this->_configFilename = $filename;
			elseif(file_exists($filename)):
				$this->_configFilename = $filename;
			else:
				$msg = sprintf("Filename %s doesn't exists.", $filename);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			throw new RuntimeException("Config file already exists and overwrite has been denied.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkConfigFile() : bool {
		//
		if(! empty($this->_configFilename)):
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfigFile() : ConfigLoaderInterface {
		//
		unset($this->_configFilename);
		
		//
		return $this;
	}
	
	/**
	 * @param    string|iterable    $source
	 * @param    NULL|callable      $handler
	 *
	 * @return null|ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings(string|iterable $source, ?callable $handler = NULL) : ?ConfigLoaderInterface {
		//
		if(class_exists($source, TRUE)):
			return $this->getSettingsFromClass($source);
		elseif(is_iterable($source) || file_exists($source)):
			return $this->getSettingsFromFile($source, $handler);
		else:
			$msg = sprintf("File %s doesn't exists and it's not class either.", $source);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string    $source
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsFromClass(string $source) : ConfigLoaderInterface {
		if(( $e = new $source() ) && $e instanceof ConfigFileInterface):
			$this->setConfig(new Config($e));
		else:
			$msg = sprintf("Config file must be an instance of %s", ConfigInterface::class);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    iterable|string    $source
	 * @param    NULL|callable      $handler
	 *
	 * @return ConfigLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsFromFile(iterable|string $source, ?callable $handler = NULL) : ConfigLoaderInterface {
		//
		if(! empty($source) && is_iterable($source)):
			$this->setConfig(new Config($source));
		elseif(is_string($source) && is_callable($handler)):
			$this->setConfig(new Config($handler($source)));
		else:
			$msg = sprintf("Source must be an iterable. Got %s.", get_debug_type($source));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
}
