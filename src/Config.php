<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Config
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Config;

//
use ArrayAccess;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Config implements ConfigInterface, ParametersPluginInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @param    ArrayAccess|iterable    $params
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(ArrayAccess|iterable $params = []) {
		if(! empty($params)):
			$this->setParams($params, TRUE);
		endif;
	}
}
